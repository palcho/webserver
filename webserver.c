#define _BSD_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <inttypes.h>
// linux libraries
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
// sockets in linux libraries
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>
#include <arpa/inet.h>

#define LOCAL_PORT 80
#define MAX_PENDING 20
#define MAX_CONNECTIONS 30
#define MAX_GRANTED_CONNECTIONS 3
#define MAX_MEMBERS 8
#define BUFFER_SIZE 1500
#define TIMEOUT 25


typedef struct {
    time_t last_seen;
    struct in_addr address;
    int socket;
    unsigned short port;
    short access_granted;
} Connection;
Connection connected[MAX_CONNECTIONS];

typedef struct {
    uint64_t username, password;
} Hash;

typedef struct {
    ssize_t size, length;
    char data[BUFFER_SIZE];
} Buffer;
Buffer msg_buffer, file_buffer;

char member_filename[MAX_GRANTED_CONNECTIONS][30];
uint64_t fj = 0; // for debugging: number of recv == -1

void jkl (int i);
void signal_handler (int signal_id);
ssize_t send_file (const char *filename, const char *http_header, int i);
ssize_t send_header (const char *header, int i);
Hash retrive_form_hash (int i);
uint64_t jenkins_hash (char *string);
const char *solve_file (void);
int matches_one_member (Hash form_hash, char *username);
void close_connection (int i);

int listen_socket, num_connected = 0, num_granted = 0;
char ordinary_buffer[30];

enum { false, true };
#define POST 0x48202f2054534f50     // order already switched
#define GET  0x5448202f20544547     // idem
#define HTTP 0x312e312f50545448     // idem
#define PATTERN 0x0a0d0a0d  // idem :: to find username and password

const char *http_200 = \
"HTTP/1.1 200 OK\r\n\
Connection: close\r\n\
Content-Length: %zu\r\n\
Content-Type: text/html\r\n\
\r\n\
%s\n";

const char *http_404 = \
"HTTP/1.1 404 Not Found\r\n\
Connection: close\r\n\
Content-Length: %zu\r\n\
Content-Type: text/html\r\n\
\r\n\
%s\n";


int main (int argc, char **argv) {
    struct sockaddr_in local_address, remote_address;
    int msg_length, answer_length, new_socket;
    socklen_t address_length = sizeof(struct sockaddr_in);
    time_t initial_time, current_time;
    FILE *file = NULL;
    Hash form_hash;
    size_t nread;

    signal(SIGINT, signal_handler);
    if ((listen_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) jkl(-1);
    fcntl(listen_socket, F_SETFL, O_NONBLOCK);
    local_address.sin_family = AF_INET;
    local_address.sin_port = htons(LOCAL_PORT);
    local_address.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(listen_socket, (struct sockaddr *)&local_address, sizeof(local_address)) < 0) jkl(-2);
    if (listen(listen_socket, MAX_PENDING) < 0) jkl(-3);
    msg_buffer.size = file_buffer.size = BUFFER_SIZE;
    initial_time = time(NULL);
    printf("\n>>> server online\n");
    
    while (1) {
        //address_length = sizeof(connected_socket[0]);
        current_time = time(NULL);
        do {
            new_socket = accept(listen_socket, (struct sockaddr *)&remote_address, &address_length);
            if (new_socket >= 0) {
                printf("\n>>> new connection %d established with %s:%"PRIu16"\n",\
                        num_connected, inet_ntoa(remote_address.sin_addr), ntohs(remote_address.sin_port));
                connected[num_connected] = (Connection) {
                    .last_seen = current_time,
                    .address = remote_address.sin_addr,
                    .socket = new_socket,
                    .port = remote_address.sin_port,
                    .access_granted = -1
                };
                fcntl(connected[num_connected].socket, F_SETFL, O_NONBLOCK);
                num_connected++;
            }
        } while (new_socket >= 0);
        for (int i = 0; i < num_connected; i++) {
            msg_buffer.length = recv(connected[i].socket, msg_buffer.data, msg_buffer.size, 0);
            if (msg_buffer.length < 0) { // no message
                if (current_time - connected[i].last_seen >= TIMEOUT) {
                    printf("\n>>> connection %d with %s:%"PRIu16" was closed by the %u sec timeout policy\n",\
                        i, inet_ntoa(connected[i].address), ntohs(connected[i].port), TIMEOUT);
                    close_connection(i);
                }
                //printf("no message received from %d %s:%"PRIu16" :: %zd\n",\
                    i, inet_ntoa(connected[i].address), ntohs(connected[i].port), msg_buffer.length);
                continue;
            }
            if (msg_buffer.length == 0) { // connection closed
                printf("\n>>> connection %d with %s:%"PRIu16" was orderly closed\n",\
                    i, inet_ntoa(connected[i].address), ntohs(connected[i].port));
                close_connection(i);
                continue;
            }
            if (msg_buffer.length == msg_buffer.size) {
                printf("\n>>> message received from %d %s:%"PRIu16" was too big. Ignoring it\n",\
                    i, inet_ntoa(connected[i].address), ntohs(connected[i].port));
                continue;
            }
            printf("\n>>> new message %d received from %s:%"PRIu16" of size %zd\n",\
                i, inet_ntoa(connected[i].address), ntohs(connected[i].port), msg_buffer.length);
            connected[i].last_seen = current_time;
            msg_buffer.data[msg_buffer.length] = '\0';
            fwrite(msg_buffer.data, sizeof(char), msg_buffer.length, stdout);
            switch (*(uint64_t *)msg_buffer.data) {
                case GET:
                    if (connected[i].access_granted > -1) {
                        // send his private file
                        send_file((const char *)member_filename[connected[i].access_granted], http_200, i);
                    } else {
                        // send the login file
                        send_file("login.html", http_200, i);
                    }
                    break;
                case POST:
                    // retrieve username and password
                    form_hash = retrive_form_hash(i);
                    // check if they match the database
                    if (matches_one_member(form_hash, ordinary_buffer)) {
                        connected[i].access_granted = num_granted;
                        sprintf(member_filename[num_granted], "%s.html", ordinary_buffer);
                        // send his private file if it does
                        send_file((const char *)member_filename[num_granted], http_200, i);
                        num_granted++;
                    } else {
                        // send the login file with a mocking message otherwise
                        send_file("otario.html", http_200, i);
                    }
                    break;
                case HTTP:
                    // he's echoing my messages
                    send_file(solve_file(), http_200, i);
                    printf("\n>>> connection %d with %s:%"PRIu16" was forcebly closed. cause: echoing\n",\
                            i, inet_ntoa(connected[i].address), ntohs(connected[i].port));
                    close_connection(i);
                    break;
                default:
                    // invalid request
                    send_file("404.html", http_404, i);
                    printf("\n>>> connection %d with %s:%"PRIu16" was forcebly closed. cause: 404\n",\
                            i, inet_ntoa(connected[i].address), ntohs(connected[i].port));
                    close_connection(i);
                    break;
            }
            /* 
            if (send(connected_socket[i], answer_msg, answer_length, 0) != answer_length) jkl(6);
            printf("message sent: %c%c%c\n", answp[0], answp[1], answp[2]);
            close(connected_socket[i]);
            connected_socket[i--] = connected_socket[--num_connected];
            printf("connection closed\n");
            */
        }
        usleep(200000);
    }
    // these last lines will never be reached
    close(listen_socket);
    for (int i = 0; i < num_connected; i++)
        close(connected[i].socket);

    return 0;
}

void signal_handler (int signal_id) {
    close(listen_socket);
    for (int i = 0; i < num_connected; i++)
        close(connected[i].socket);
    printf("\n>>> closed all %d connections\n", num_connected);
    exit(1);
}

void jkl (int i) {
    fprintf(stderr, "\n>>> ERROR: %d: %s (errno %d)\n", i, strerror(errno), errno);
    if (i < 0) exit(i);
}

ssize_t send_file (const char *filename, const char *http_header, int i) {
    ssize_t nsent;
    FILE *file = fopen(filename, "r");
    if (!file) {
        fprintf(stderr, "\n>>> could not open file %s for %d %s:%"PRIu16"\n",\
            filename, i, inet_ntoa(connected[i].address), ntohs(connected[i].port));
        return 0;
    }
    file_buffer.length = fread(file_buffer.data, sizeof(char), file_buffer.size, file);
    msg_buffer.length = snprintf(msg_buffer.data, msg_buffer.size, http_header, file_buffer.length, file_buffer.data);
    nsent = send(connected[i].socket, msg_buffer.data, msg_buffer.length, MSG_NOSIGNAL);
    if (nsent != msg_buffer.length) {
        fprintf(stderr, "\n>>> error sending file %s to %d %s:%"PRIu16" | nsent = %zd, but length = %zu\n",\
            filename, i, inet_ntoa(connected[i].address), ntohs(connected[i].port), nsent, file_buffer.length);
        fclose(file);
        return 0;
    }
    printf("\n>>> sent file %s to %d %s:%"PRIu16" successfully | nsent = %zd\n",\
        filename, i, inet_ntoa(connected[i].address), ntohs(connected[i].port), nsent);
    fclose(file);
    return nsent;
}
/*
ssize_t send_header (const char *header, int i) {
    ssize_t nsent, length;
    length = strlen(header);
    nsent = send(connected[i].socket, header, length, MSG_NOSIGNAL);
    if (nsent != length) {
        fprintf(stderr, "\n>>> error sending http header to %d %s:%"PRIu16" | nsent = %zd, but length = %zu\n",\
            i, inet_ntoa(connected[i].address), ntohs(connected[i].port), nsent, file_buffer.length);
        return 0;
    }
    printf("\n>>> sent http header to %d %s:%"PRIu16" successfully | nsent = %zd\n",\
        i, inet_ntoa(connected[i].address), ntohs(connected[i].port), nsent);
    return nsent;
}
*/
Hash retrive_form_hash (int i) {
    Hash form_hash;
    char *string, *pswptr;
    string = strstr(msg_buffer.data, "\r\n\r\n");
    if (!string) {
        fprintf(stderr, "\n>>> no end of header marker found in message from %d %s:%"PRIu16"\n",\
            i, inet_ntoa(connected[i].address), ntohs(connected[i].port));
        return (Hash){ 0, 0 };
    }
    string = strstr(string, "username=") + 9;
    if ((uintptr_t)string <= 10) goto error_handler;
    pswptr = strstr(string, "&password=") + 10;
    if ((uintptr_t)pswptr <= 11) goto error_handler;
    *(pswptr - 10) = '\0';
    form_hash = (Hash) {
        .username = jenkins_hash(string),
        .password = jenkins_hash(pswptr)
    };
    printf("\n>>> hash from %d %s:%"PRIu16" form { %s, %s } = { 0x%016"PRIx64",  0x%016"PRIx64" }\n",
        i, inet_ntoa(connected[i].address), ntohs(connected[i].port), string, pswptr, form_hash.username, form_hash.password);
    return form_hash;

error_handler:
    fprintf(stderr, "\n>>> no username and password found in message from %d %s:%"PRIu16"\n",\
        i, inet_ntoa(connected[i].address), ntohs(connected[i].port));
    return (Hash){ 0, 0 };
}

uint64_t jenkins_hash (char *string) {
    uint64_t hash = 0;
    do {
        hash += *string;
        hash += hash << 10;
        hash ^= hash >> 6;
    } while (*string++);
    hash += hash << 3;
    hash ^= hash >> 11;
    hash += hash << 15;
    return hash;
}

const char *solve_file (void) {
    char *string;
    string = strstr(msg_buffer.data, "<title>") + 7;
    if ((uintptr_t)string <= 8)
        return "404.html";
    if (*string == 'o')
        return "otario.html";
    if (*string == 'l')
        return "login.html";
    return "login.html";
}

int matches_one_member (Hash form_hash, char *username) {
    FILE *file = fopen("members.txt", "r");
    char password[30];
    if (!file) jkl(-8);
    for (int i = 0; fscanf(file, " %[^\n] %[^\n]", username, password) != EOF; i++) {
        if (form_hash.username == jenkins_hash(username) \
        &&  form_hash.password == jenkins_hash(password)) {
            fclose(file);
            return true;
        }
    }
    fclose(file);
    return false;
}

void close_connection (int i) {
    close(connected[i].socket);
    if (connected[i].access_granted)
        num_granted--;
    connected[i--] = connected[--num_connected];
}
